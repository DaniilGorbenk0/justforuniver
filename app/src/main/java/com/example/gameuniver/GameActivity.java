package com.example.gameuniver;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.View;
import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Window;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import static android.graphics.Color.GREEN;

public class GameActivity extends Activity{
    boolean lose = false;
    Settings settings;
    boolean drawOnce;
    int score;
    int errors;
    int speed;
    int chuvstv;
    int intens;
    int sizeText;
    int waitNewElem = 0;
    int waitTimer = 0;
    int endTimer;
    int startTimer;
    private Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private float initX, initY;
    private float targetX, targetY;
    private boolean drawing = true;
    private Basket basket;
    private int winWidth;
    private int winHeight;
    int changeBasketPosition = 0;
    double chanceNewElemnt;
    double minChanceNewElement;
    Paint p;
    boolean canDraw;
    ArrayList<element> elems;
    SensorManager sensorManager;
    Sensor sensorAccel;
    StringBuilder sb = new StringBuilder();
    Timer timer;
    int mnozh;

    MySurfaceThread thread;
    boolean myThreadRun = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        drawOnce = true;
        score = 0;
        canDraw = true;
        settings = Settings.getInstance();
        mnozh = settings.chuvstv+settings.intens+settings.speed-settings.errors;
        if (mnozh < 1){
            mnozh = 1;
        }
        sizeText = 0;
        errors = settings.errors;
        speed = settings.speed;
        chuvstv = settings.chuvstv;
        intens = settings.intens;
        minChanceNewElement = intens*0.1;
        endTimer = (int)(500/(intens*speed));
        startTimer = (int)(100/(intens*speed));
        // датчики
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensorAccel = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);


        // размер экрана
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        winWidth = size.x;
        winHeight = size.y;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // список элементов и корзина
        elems = new ArrayList<element>();

        //int choise = (int)(Math.random()*4);
        double choise = Math.random();
        Resources res = getResources();
        Bitmap bitmap;
        if (choise >= 0.8){
            bitmap = BitmapFactory.decodeResource(res, R.drawable.box_4);
        }
        else if (choise >= 0.6){
            bitmap = BitmapFactory.decodeResource(res, R.drawable.box_3);
        }
        else if (choise >= 0.4){
            bitmap = BitmapFactory.decodeResource(res, R.drawable.box_2);
        }
        else{
            bitmap = BitmapFactory.decodeResource(res, R.drawable.box_1);
        }
        basket = new Basket(bitmap, (int)winWidth/2, winHeight-220, 64);
        setContentView(new MySurfaceView(this));
    }


    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        try {
            myThreadRun = false;
            thread.join();
            Intent intent = new Intent(GameActivity.this, MainActivity.class);
            startActivity(intent);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(listener, sensorAccel,
                SensorManager.SENSOR_DELAY_NORMAL);

        timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (valuesAccel[0] > 0){
                            setChangeBasketPosition(-1*(int)(valuesAccel[0]+1+chuvstv));
                            //changeBasketPosition = -1*(int)(valuesAccel[0]+1+chuvstv);
                        }
                        else{
                            setChangeBasketPosition((int)(-1*valuesAccel[0]+1+chuvstv));
                            //changeBasketPosition = (int)(-1*valuesAccel[0]+1+chuvstv);
                        }
                    }
                });
            }
        };
        timer.schedule(task, 0, 1);
    }

    public synchronized void setChangeBasketPosition(int pos){
        changeBasketPosition = pos;
    }

    float[] valuesAccel = new float[3];

    SensorEventListener listener = new SensorEventListener() {

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            switch (event.sensor.getType()) {
                case Sensor.TYPE_ACCELEROMETER:
                    for (int i = 0; i < 3; i++) {
                        valuesAccel[i] = event.values[i];
                    }
                    break;

            }

        }

    };

    // класс одного элемента
    public class element {
        //circle - круг, х,у - координаты
        int x;
        int y;
        Bitmap bitmap;
        element(Bitmap bitmap, int x, int y){
            this.bitmap = bitmap;
            this.x = x;
            this.y = y;
        }

    }
    public class Basket {
        // класс для корзинки
        int x;
        int y;
        int width;
        Bitmap bitmap;

        Basket(Bitmap bitmap, int x, int y, int w){
            this.bitmap = bitmap;
            this.x = x;
            this.y = y;

            this.width = w;
        }

    }
    public class MySurfaceThread extends Thread {

        private SurfaceHolder myThreadSurfaceHolder;
        private MySurfaceView myThreadSurfaceView;

        public MySurfaceThread(SurfaceHolder surfaceHolder,
                               MySurfaceView surfaceView) {
            myThreadSurfaceHolder = surfaceHolder;
            myThreadSurfaceView = surfaceView;
        }

        public void setRunning(boolean b) {
            myThreadRun = b;
        }

        @Override
        public void run() {
            // super.run();
            while (myThreadRun) {
                //waitTimer ++;

                Canvas c = null;
                try {

                    synchronized (myThreadSurfaceHolder) {
                        c = myThreadSurfaceHolder.lockCanvas(null);
                        myThreadSurfaceView.onDraw(c);
                    }

                    sleep(1);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {

                    if (c != null) {
                        myThreadSurfaceHolder.unlockCanvasAndPost(c);
                    }
                }

            }
        }
    }

    public class MySurfaceView extends SurfaceView implements
            SurfaceHolder.Callback {

        @SuppressLint("DrawAllocation")
        @Override
        protected void onDraw(Canvas canvas) {
            if (errors < 0){
                canDraw = false;
            }
            else{
                canDraw = true;
            }
            if (canDraw) {
                // добавляем новый элемент
                chanceNewElemnt = Math.random();
                waitTimer++;
                if (elems.size() < settings.maxElem && waitNewElem <= waitTimer) {
                    int x = (int) (Math.random() * (winWidth - 40) + 40);
                    Resources res = getResources();
                    Bitmap bitmap;
                    double choise = Math.random();
                    if (choise >= 0.8) {
                        bitmap = BitmapFactory.decodeResource(res, R.drawable.ball_5);
                    } else if (choise >= 0.6) {
                        bitmap = BitmapFactory.decodeResource(res, R.drawable.ball_4);
                    } else if (choise >= 0.4) {
                        bitmap = BitmapFactory.decodeResource(res, R.drawable.ball_3);
                    } else if (choise >= 0.2) {
                        bitmap = BitmapFactory.decodeResource(res, R.drawable.ball_2);
                    } else {
                        bitmap = BitmapFactory.decodeResource(res, R.drawable.ball_1);
                    }

                    element newElement = new element(bitmap, x, 25);
                    elems.add(newElement);
                    waitNewElem = (int) (Math.random() * (endTimer - startTimer) + startTimer);
                    waitTimer = 0;
                }

                if (basket.x < winWidth - basket.width && changeBasketPosition > 0) {
                    basket.x += changeBasketPosition;
                } else if ((basket.x > 0 && changeBasketPosition < 0)) {
                    basket.x += changeBasketPosition;
                }

                // рассчитываем попал ли кто-то в корзину
                int startX = basket.x;
                int endX = basket.x + basket.width;
                int startY = basket.y;
                ArrayList<Integer> removeListGood = new ArrayList<Integer>();
                ArrayList<Integer> removeListBad = new ArrayList<Integer>();
                for (int i = 0; i < elems.size(); i++) {
                    element el = elems.get(i);
                    if (el.y > startY && el.x > startX && el.x < endX) {
                        removeListGood.add(i);
                    } else if (el.y > winHeight) {
                        removeListBad.add(i);
                    }
                }
                int plus = 0;
                for (int i = 0; i < removeListGood.size(); i++) {
                    elems.remove(removeListGood.get(i) - plus);
                    plus++;
                    score++;
                }

                for (int i = 0; i < removeListBad.size(); i++) {
                    elems.remove(removeListBad.get(i) - plus);
                    plus++;
                    errors--;
                }

                // super.onDraw(canvas);
                canvas.drawRGB(0, 0, 0);
                // добавляем элемент для отрисовки
                p = new Paint();
                p.setColor(Color.RED);
                p.setStrokeWidth(10);
                // рисуем корзинку
                canvas.drawBitmap(basket.bitmap, basket.x, basket.y, p);
                //отрисовка элементов
                for (int i = 0; i < elems.size(); i++) {
                    element el = elems.get(i);
                    el.y += speed;
                    canvas.drawBitmap(el.bitmap, el.x, el.y, p);
                }

                p.setTextSize(40);
                canvas.drawText("Счет: " + String.valueOf(score), 10, 33, p);
                canvas.drawText("Жизни: " + errors, (int) winWidth / 2, 33, p);
            } else{
                canvas.drawRGB(0, 0, 0);
                p = new Paint();
                //p.setColor(Color.RED);
                if (sizeText < 49){
                    sizeText++;
                }

                p.setTextSize(sizeText);
                p.setColor(Color.WHITE);
                //canvas.drawText("Вы проиграли", 200,500,p);
                if (drawOnce) {
                    score = (int) score * mnozh;
                    @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss").format(Calendar.getInstance().getTime());
                    settings.DBB.addNewScoreForUser(settings.id_user, score, timeStamp);
                }
                drawOnce = false;
                p.setTextSize(96 - sizeText);
                canvas.drawText("Вы проиграли!", 150, 500, p);
                p.setTextSize(sizeText);
                canvas.drawText("Бонус множитель: " + mnozh, 150, 550, p);
                p.setTextSize(96 - sizeText);
                canvas.drawText("Итоговый счет: " + score, 150, 600, p);


            }
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            // return super.onTouchEvent(event);
            int action = event.getAction();

            if (action == MotionEvent.ACTION_DOWN) {

                if (!canDraw){
                    canDraw = true;
                    errors = settings.errors;
                    elems = new ArrayList<element>();
                    score = 0;
                    drawOnce = true;
                    sizeText = 0;
                }

                Toast toast = Toast.makeText(getApplicationContext(),
                        "CLICK", Toast.LENGTH_SHORT);
                toast.show();
                targetX = event.getX();
                targetY = event.getY();
                drawing = true;

            }

            return true;
        }

        public MySurfaceView(Context context) {
            super(context);
            init();
        }

        public MySurfaceView(Context context, AttributeSet attrs) {
            super(context, attrs);
            init();
        }

        public MySurfaceView(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
            init();
        }

        private void init() {
            getHolder().addCallback(this);
            thread = new MySurfaceThread(getHolder(), this);

            setFocusable(true); // make sure we get key events

            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(3);
            paint.setColor(Color.WHITE);

            initX = targetX = 0;
            initY = targetY = 0;

        }

        @Override
        public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2,
                                   int arg3) {
        }

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            thread.setRunning(true);
            thread.start();
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            boolean retry = true;
            thread.setRunning(false);
            while (retry) {
                try {
                    thread.join();
                    retry = false;
                } catch (InterruptedException e) {
                }
            }
        }
    }
}