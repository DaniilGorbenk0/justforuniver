package com.example.gameuniver;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MenuActivity extends AppCompatActivity {
    /*
        Активити отвечающее за игровое меню
     */
    // текст для отображения счета
    TextView maxScore;
    // класс настроек
    Settings settings;
    // виджет для подгрузки изображения
    ImageView img1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // метод, вызывающийся при создании активити
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        maxScore = (TextView)findViewById(R.id.best_score_text);
        img1 = findViewById(R.id.img3);
        img1.setImageResource(R.drawable.img2);
        settings = Settings.getInstance();
        // делаем запрос в БД для получения максимального значения
        Cursor query = settings.DBB.getBestUserScore(settings.id_user);
        // выводим на экран максимальное значение очков
        if (query.moveToFirst() && query.getCount()>0) {
            int bestS = query.getInt(1);
            maxScore.setText(String.valueOf(bestS));
        }
    }
    @SuppressLint("SetTextI18n")
    public void setMaxScore() {
        // метод для отображения количества очков
        int score = 0;
        maxScore.setText("Ваш лучший счет: " + score);
    }

    public void goToSettings(View view) {
        // метод для перехода в активити "Настройки"
        Intent intent = new Intent(MenuActivity.this, SettingsActivity.class);
        startActivity(intent);
    }


    public void goToGame(View view) {
        // метод для перехода в активити "игра"
        Intent intent = new Intent(MenuActivity.this, GameActivity.class);
        startActivity(intent);
    }

    public void goToScores(View view) {
        // метод для перехода в активити "таблица счетов"
        Intent intent = new Intent(MenuActivity.this, ScoreActivity.class);
        startActivity(intent);
    }
}

