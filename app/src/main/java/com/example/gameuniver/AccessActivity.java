package com.example.gameuniver;


import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class AccessActivity extends AppCompatActivity {
    /*
    Класс авторизации пользователя
     */
    ImageView img1;
    EditText log;
    EditText pwd;
    Settings settings;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_access);
        // Привязка Вьюшек к переменным
        img1 = findViewById(R.id.img1);
        img1.setImageResource(R.drawable.img2);
        log = findViewById(R.id.inp_log);
        pwd = findViewById(R.id.inp_pas);
        settings = Settings.getInstance();
        SQLiteDatabase db = getBaseContext().openOrCreateDatabase("app104.db", MODE_PRIVATE, null);
        settings.setDB(db);
    }

    public void goCreateAccount(View view) {
        // метод для перехода в регистрацию
        Intent intent = new Intent(AccessActivity.this, CreateActivity.class);
        startActivity(intent);
    }

    public void inputLogin(View view) {
        // метод нажатия кнопки "Вход"
        // считываем введенные данные
        String password = pwd.getText().toString();
        String login = log.getText().toString();
        // проверка регистрации пользователя
        Cursor query = settings.DBB.checkLoginAndPassword(login, password);
        if (query.getCount() == 0){
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Неверный логин и/или пароль", Toast.LENGTH_SHORT);
            toast.show();
        }
        else{
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Вы успешно авторизованы", Toast.LENGTH_SHORT);
            toast.show();
            if (query.moveToFirst()) {
                settings.nick = query.getString(2);
                settings.id_user = query.getInt(0);
            }
            Intent intent = new Intent(AccessActivity.this, MainActivity.class);
            startActivity(intent);
        }
    }
}

