package com.example.gameuniver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class CreateActivity extends AppCompatActivity {
    /*
    Класс регистрации пользователя
     */
    ImageView img1;
    EditText log;
    EditText pas;
    Settings settings;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // привязка вьюшек к переменным
        setContentView(R.layout.activity_create);
        img1 = findViewById(R.id.img2);
        img1.setImageResource(R.drawable.img2);
        log = findViewById(R.id.new_nick);
        pas = findViewById(R.id.new_pas);
        settings = Settings.getInstance();
    }

    public void goRegistration(View view) {
        // Метод регистрации пользователя
        String login = log.getText().toString();
        String password = pas.getText().toString();
        // Проверка на существующий логин
        Cursor query = settings.DBB.checkUser(login);
        if (query.getCount() == 0){
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Вы успешно зарегистрировались", Toast.LENGTH_SHORT);
            toast.show();
            // добавление нового пользователя
            settings.DBB.addNewUser(login, password);
            Intent intent = new Intent(CreateActivity.this, AccessActivity.class);
            startActivity(intent);
        }
        else{
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Такой логин уже существует", Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}
