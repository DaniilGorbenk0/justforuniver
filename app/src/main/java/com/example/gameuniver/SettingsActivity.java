package com.example.gameuniver;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.SeekBar;
import android.widget.TextView;

public class SettingsActivity extends AppCompatActivity {
    /*
        Класс отвечающий за настройки игры
     */
    private TextView speedTextView;
    private TextView intensTextView;
    private TextView chuvstvTextView;
    private TextView errorsTextView;
    private TextView elemsTextView;
    Settings settings;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        //класс для хранения настроек
        settings = Settings.getInstance();
        // отображаем вывод текста СКОРОСТЬ:
        speedTextView = (TextView)findViewById(R.id.text_speed);
        speedTextView.setText("Скорость: " + String.valueOf(settings.speed));
        // метод для изменения значения "Скорость"
        final SeekBar seekBarSpeed = (SeekBar)findViewById(R.id.seekbar_speed);
        seekBarSpeed.setProgress(settings.speed);
        seekBarSpeed.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @SuppressLint("SetTextI18n")
            @Override public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                int speed = (int) seekBarSpeed.getProgress();
                speedTextView.setText("Скорость: " + String.valueOf(speed));

                settings.speed = speed;

            }

            @Override public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        // отображаем вывод текста ИНТЕНСИВНОСТЬ:
        intensTextView = (TextView)findViewById(R.id.text_intens);
        intensTextView.setText("Интенсивность: " + settings.intens);
        // метод для изменения значения "ИНтенсивность"
        final SeekBar seekBarIntens = (SeekBar)findViewById(R.id.seekbar_intens);
        seekBarIntens.setProgress(settings.intens);
        seekBarIntens.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @SuppressLint("SetTextI18n")
            @Override public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                int intens = (int) seekBarIntens.getProgress();
                intensTextView.setText("Интенсивность: " + String.valueOf(intens));
                settings.intens = intens;
            }

            @Override public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        // отображаем вывод текста ЧУВСТВИТЕЛЬНОСТ:
        chuvstvTextView = (TextView)findViewById(R.id.text_chuvstv);
        chuvstvTextView.setText("Чувствительность: " + settings.chuvstv);
        // метод для изменения значения "Чувствительность"
        final SeekBar seekBarChuvstv = (SeekBar)findViewById(R.id.seekbar_chuvstv);
        seekBarChuvstv.setProgress(settings.chuvstv);
        seekBarChuvstv.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @SuppressLint("SetTextI18n")
            @Override public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                int chuvstv = (int) seekBarChuvstv.getProgress();
                chuvstvTextView.setText("Чувствительность: " + String.valueOf(chuvstv));

                settings.chuvstv = chuvstv;
            }

            @Override public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        // отображаем вывод текста ОШИБОК:
        errorsTextView = (TextView)findViewById(R.id.text_errors);
        errorsTextView.setText("Ошибок: " + settings.errors);
        // метод для изменения значения "Ошибки"
        final SeekBar seekBarErrors = (SeekBar)findViewById(R.id.seekbar_errors);
        seekBarErrors.setProgress(settings.errors);
        seekBarErrors.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @SuppressLint("SetTextI18n")
            @Override public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                int errors = (int) seekBarErrors.getProgress();
                errorsTextView.setText("Ошибок: " + String.valueOf(errors));
                settings.errors = errors;
            }

            @Override public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });



        // отображаем вывод текста ЭЛЕМЕНТЫ:
        elemsTextView = (TextView)findViewById(R.id.text_num_elems);
        elemsTextView.setText("Элементов: " + settings.maxElem);
        // метод для изменения значения "Кол-во элементов"
        final SeekBar seekBarElems = (SeekBar)findViewById(R.id.seekbar_elems);
        seekBarElems.setProgress(settings.maxElem);
        seekBarElems.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @SuppressLint("SetTextI18n")
            @Override public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                int elems = (int) seekBarElems.getProgress();
                elemsTextView.setText("Элементов: " + String.valueOf(elems));
                settings.maxElem = elems;
            }

            @Override public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });



    }
}
