package com.example.gameuniver;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class ScoreActivity extends AppCompatActivity {
    /*
    Класс для отображения очков пользователя
     */
    Settings settings;
    ListView listView;
    ImageView img1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);
        listView = (ListView)findViewById(R.id.list_view_scores);
        settings = Settings.getInstance();
        img1 = findViewById(R.id.img4);
        img1.setImageResource(R.drawable.img2);
        // получить все очки пользователя
        ArrayList<String> array = new ArrayList<String>();
        array = settings.DBB.getAllScoresForUser(settings.id_user);
        // вывести на экран
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, array);
        listView.setAdapter(adapter);
    }
}