package com.example.gameuniver;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class DBbackgound {
    /*
        Класс для работы с запросами к БД
     */
    // экзеспляр класса БД
    SQLiteDatabase db;
    public void setDb(SQLiteDatabase db1){
        this.db = db1;
    }
    public void createTableScores(){
        // создание таблицы "Счет"
        this.db.execSQL("CREATE TABLE IF NOT EXISTS scores (id integer primary key ,score int, time text, id_user int)");
    }
    public void createTableUsers(){
        // создание таблицы "Юзеры"
        db.execSQL("CREATE TABLE IF NOT EXISTS users (id_user integer primary key ,loginu text,passwordu text)");
    }
    public Cursor checkUser(String login){
        // Проверка на существование введеного логина
        String sql = "select * from users where loginu='" + login + "'";
        Cursor query = this.db.rawQuery(sql, null);
        return query;
    }
    public Cursor checkLoginAndPassword(String login, String password){
        // Проверка на существование логина и пароля
        String sql = "select * from users where loginu='" + login + "' and passwordu='" + password + "'";
        Cursor query = this.db.rawQuery(sql, null);
        return  query;
    }
    public void addNewUser(String login, String password){
        // Добавление нового Юзера в БД
        String sql = "insert into users (loginu, passwordu) values ('" + login + "','" + password +"')";
        this.db.execSQL(sql);
    }
    public Cursor getBestUserScore(int id_user){
        // Получить максимальное значение очков пользователя
        String sql = "select * from scores where id_user = '"+id_user+"' order by score desc limit 1";
        Cursor query = this.db.rawQuery(sql, null);
        return query;
    }
    public void addNewScoreForUser(int id_user, int score, String timeStamp){
        // Добавить новый результат игры
        String sql = "insert into scores (id_user ,score, time) values ("+ id_user+", " + score + ",'" + timeStamp + "')";
        this.db.execSQL(sql);
    }
    public ArrayList<String>  getAllScoresForUser(int id_user){
        // Получить все результаты игр пользователя
        String sql = "select * from scores where id_user = " + id_user;
        Cursor query = this.db.rawQuery(sql, null);
        ArrayList<String> array = new ArrayList<String>();
        query.moveToFirst();
        while (query.moveToNext()){
            int scoreOne = query.getInt(1);
            String mytime = query.getString(2);
            String rez = "Счет: " + String.valueOf(scoreOne) + ".    Время: " + mytime;
            array.add(rez);
        }
        return array;
    }
}
