package com.example.gameuniver;

import android.database.sqlite.SQLiteDatabase;

public class Settings {
    /*
    Класс-одиночка для хранения переменных
     */
    // игровые переменные, которые были определены в классе настроек
    int errors=3;
    int speed=1;
    int intens=1;
    int chuvstv=1;
    int maxElem=15;
    int id_user=0;
    DBbackgound DBB;
    String nick="";
    SQLiteDatabase db;
    private static Settings instance;
    private Settings(){
        this.DBB = new DBbackgound();
    }
    public static Settings getInstance(){ // #3
        if(instance == null){		//если объект еще не создан
            instance = new Settings();	//создать новый объект
        }
        return instance;		// вернуть ранее созданный объект
    }
    public void setDB(SQLiteDatabase db){
        // метод для подключения к БД
        this.db = db;
        DBB.db = db;
        DBB.createTableScores();
        DBB.createTableUsers();
    }
}
